# Dockerfile, Repositorio y Balanceador



Nombre y Apellido: Matias Nicolas Minar

Materia: Infraestructura en la nube 

Profesor: Sergio Pernas

## Introducción

Desarrollo e implementación de dos servidores Docker con un servidor balanceador nginx. todo esto conectado al repositorio docker-git en gitlab.

## Requisitos 

Se generan 3 instancias virtualizadas localmente con VirtualBox. 2 servidores Docker y 1 servidor Nginx 



### Sistema operativo y red

\- servidor Ubuntu 22.2  

- Red NAT (VPC) 200.0.0.0/24 

- docker IP 200.0.0.50

- docker2 IP 200.0.0.40

- nginx IP 200.0.0.10

  

### Almacenamiento 

Volumen sda1 para directorio raiz



## Desarrollo





## Paso 1

Lo primero es iniciar sesión en gitlab.com. alli vamos a crear un proyeco y cargar la llave ssh dentro del proyecto para lograr la conexion con las VM. luego copiamos el codigo de copiado el proyecto y lo dejamos guardado en un bloc de notas, lo vamos a usar despues. 



## Paso 2

hecho esto vamos a crear el directorio develop 

`git checkout -b develop`

puesto el comando ya te lleva a la rama develop 



## Paso 3

En el servidor Docker, cambiamos de usuario al usuario root y copiamos el repositorio de Gitlab. 

el repositorio hay que copiarlo en el directorio `/home/istea` y lo hacemos con el siguiente comando 

`git clone git@gitlab.com:matias.minar/docker-git.git`

comprobemos que se haya clonado con exito, deberia aparecer el directorio `docker-git` para eso ponemos el comando `ls`

![image-20240624142247963](C:\Users\matia\AppData\Roaming\Typora\typora-user-images\image-20240624142247963.png)

Hecho esto vamos a configurar el usuario de git, si no lo hacemos no nos va a dejar subir las actualizaciones al repositorio, estos son los comandos

`git config user.name "John Doe"`

`git config user.email john@doe.com`



## Paso 4



Ahora parados en el directorio `/home/istea/` vamos a descargar todas las dependencias que vamos a necesitar con este comando 

`wget  https://github.com/twbs/bootstrap/releases/download/v5.3.3/bootstrap-5.3.3-examples.zip `

`wget https://gitlab.com/sergio.pernas1/album-jueves/-/raw/main/apache2.conf `

Ahora vamos a descomprimir con unzip, de no tenerlo hay que descargarlo con `apt install unzip`

`unzip bootstrap-5.3.3-examples.zip`

Ahora movemos la app a un directorio que nombraremos html

`mv bootstrap-5.3.3-examples/docker-git html`

una vez que tenemos los archivos copiados en el directorio html borramos el archivo zip con `rm bootstrap-5.3.3-examples.zip` y el directorio con `rm -r bootstrap-5.3.3-examples`



Con esta aplicacion vamos a necesitar cambiar el archivo `index.html` de lugar. con este comando 

`mv docker-git/html/album/index.html docker-git/html`



## Paso 5

vamos a formatear y montar un volumen. lo agregamos en la vm con las herramientas del virtualizador 

una vez hecho esto vamos a ver que etiqueta se le agrego con `lsblk` y en este caso se le asigno la etiqueta `sdb`

lo formateamos con `mkfs.xfs /dev/sdb`

hecho esto vamos a necesitar conocer el UUID del volumen con este comando `lsblk -f /dev/sdb`

configuramos el montaje automatico con systemd 

`mkdir /mnt/Docker_vol`

y agregamos el fichero de configuración 

`nano /lib/systemd/system/mnt-docker_vol.mount`

y agregamos lo siguiente 

`[Unit]`
`Description=Montaje de la partición de Datos en /mnt/docker_vol`
`After=network.target`
`[Mount]`
`What=UUID=63dd92fa-4c2e-4717-b6db-6304fd1ab7ac`

`Where=/mnt/docker_vol`
`Type=xfs`
`Options=defaults`
`[Install]`
`WantedBy=multi-user.target`

y guardamos 

activamos el punto de montaje con 

`systemctl enable --now mnt-docker_vol.mount`

y verificamos con 

`lsblk /dev/sdb`

![image-20240710135438222](C:\Users\matia\AppData\Roaming\Typora\typora-user-images\image-20240710135438222.png)

los ultimos pasos son crear el fichero deamon 

`nano /etc/docker/daemon.json`

y agregamos lo siguiente

`{
"data-root": "/mnt/docker_vol"
}`

reiniciamos los servicios 
 `systemctl restart docker`
`systemctl restart docker.socket`
Creamos un volumen
`docker volume create vol-1`
`docker volume ls`

![volumenn](C:\Users\matia\OneDrive\Escritorio\volumenn.png)



y por ultimo verificamos todo 

![volumen](C:\Users\matia\OneDrive\Escritorio\volumen.png)





## Paso 6

Parados en `/home/istea/docker-git` creemos el archivo Dockerfile para iniciar con el servicio Docker

`nano Dockerfile`

y le ponemos este texto adentro 

![image-20240624145042258](C:\Users\matia\AppData\Roaming\Typora\typora-user-images\image-20240624145042258.png)

ahora armemos la imagen Docker y lancemos el contenedor 

`docker build -t album .`

`docker run -d --name mati -p 8080:80 album`





## Paso 7

Ahora parados en el directorio `/home/istea/docker-git` vamos a actualizar el repositorio git  con estos comandos 

`git add .`

`git commit -m "esto es un comentario"`

`git push origin main`

`git push origin develop`

si todo salio bien deberias ver lo mismo en el directorio `/home/istea/docker-git` y en Gitlab 

![image-20240624144305017](C:\Users\matia\AppData\Roaming\Typora\typora-user-images\image-20240624144305017.png)

![image-20240624144332528](C:\Users\matia\AppData\Roaming\Typora\typora-user-images\image-20240624144332528.png)

como vemos que tenemos los mismos archivos y carpetas.  con esto ya tenemos el sistema docker funcionando 



## Paso 8



en una 2da VM dentro del directorio `/etc/nginx/sites-available` configuramos Vhost Nginx y balanceo creando el siguiente archivo

`nano sitio.net.conf`

y escribimos lo siguiente

![image-20240624150942686](C:\Users\matia\AppData\Roaming\Typora\typora-user-images\image-20240624150942686.png)



una vez hecho esto vamos a linkear el archivo con la carpeta `sites-enabled` con este comando 

`ln sites-available/sitio.conf.net /sites-enabled/`

Y recargamos el servicio con `systemctl reload nginx.service`

por ultimo agregamos el host en la maquina cliente 



## Paso 9



Documentamos todo en el archivo README.md y actualizamos el repositorio 